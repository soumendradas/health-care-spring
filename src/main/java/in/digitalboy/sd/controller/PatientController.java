package in.digitalboy.sd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.digitalboy.sd.dto.PatientDto;
import in.digitalboy.sd.service.IPatientService;

@RestController
@RequestMapping("patient")
public class PatientController {
	
	@Autowired
	private IPatientService patientService;
	
	@PostMapping("/save")
	public ResponseEntity<String> savePatient(@RequestBody PatientDto patient) {
		System.out.println(patient);
		Long id = patientService.savePatient(patient);
		String message = "Patient '"+id+"' created";
		
		return ResponseEntity.ok(message);
	}
	
	@GetMapping("/all")
	public ResponseEntity<List<PatientDto>> showAllPatient() {
		
		List<PatientDto> patients= patientService.getAllPatient();
		
		return ResponseEntity.ok(patients);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<String> deletePatient(@PathVariable Long id) {
		
		patientService.removePatient(id);
		
		String message = "Patient '"+id+"' deleted";
		
		return ResponseEntity.ok(message);
	}
	
	@GetMapping("/getPatient/{id}")
	public ResponseEntity<PatientDto> getPatient(@PathVariable Long id) {
		PatientDto patient = patientService.getOnePatient(id);
		
		return ResponseEntity.ok(patient);
	}
	
	@PutMapping("/update")
	public ResponseEntity<String> updatePatient(@RequestBody PatientDto patient) {
		
		String msg = patientService.updatePatient(patient);
		
		return ResponseEntity.ok(msg);
	}

}
