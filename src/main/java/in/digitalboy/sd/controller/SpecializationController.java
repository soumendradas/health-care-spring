package in.digitalboy.sd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import in.digitalboy.sd.dto.SpecializationDto;
import in.digitalboy.sd.entity.Specialization;
import in.digitalboy.sd.service.ISpecializationService;

@RestController
@RequestMapping("/spec")
public class SpecializationController {
	
	@Autowired
	private ISpecializationService service;
	
//	@GetMapping("/register")
//	public String showSpecilizationRegisterPage() {
//		
//		
//		return "specialization/registerPage";
//	}
//	
	@PostMapping("/save")
	public ResponseEntity<String> saveSpecialization(@RequestBody SpecializationDto specialization) {
		
		Integer id = service.saveSpec(specialization);
		
		String message = "Specialization '"+id+"' created";
		
		return ResponseEntity.ok(message);
	}
	
	@GetMapping("/all")
	public ResponseEntity<List<SpecializationDto>> viewAllSpecialization(Model model) {
		List<SpecializationDto> allSpecialization = service.getAllSpec();
		
		return ResponseEntity.ok(allSpecialization);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<String> specDelete(@PathVariable Integer id) {
		
		service.removeSpec(id);
		String message = "Specialization '"+id+"' deleted";
		
		return ResponseEntity.ok(message);
	}
	
	@GetMapping("/edit/{id}")
	public ResponseEntity<SpecializationDto> showSpecEditPage(@PathVariable Integer id) {
		
		SpecializationDto spec = service.getOneSpec(id);
		return ResponseEntity.ok(spec);
	}
	
	@PutMapping("/update")
	public ResponseEntity<String> specUpdate(@RequestBody SpecializationDto spec) {
		
		String msg = service.updateSpec(spec);
		
		return ResponseEntity.ok(msg);
	}
	
	@GetMapping("checkCode")
	public ResponseEntity<String> checkCode(@RequestParam Integer id,
			@RequestParam String code){
		
		String msg = "";
		
		if(service.countSpecCode(id, code)) {
			msg = "Specialization Code is Exist";
		}
		
		return ResponseEntity.ok(msg);
	}
	
	@GetMapping("checkName")
	public ResponseEntity<String> checkName(@RequestParam Integer id,
			@RequestParam String name){
		
		
		String msg = "";
		if(service.countSpecName(id, name)) {
			msg = "Specialization Name is exist";
		}
		
		return ResponseEntity.ok(msg);
	}

}
