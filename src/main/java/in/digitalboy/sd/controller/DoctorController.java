package in.digitalboy.sd.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import in.digitalboy.sd.dto.DoctorDto;
import in.digitalboy.sd.dto.SpecializationDto;
import in.digitalboy.sd.service.IDoctorService;
import in.digitalboy.sd.service.ISpecializationService;

@Controller
@RequestMapping("/doctor")
public class DoctorController {
	@Autowired
	private IDoctorService doctorService;
	
	@Autowired
	private ISpecializationService specService;
	
	@GetMapping("/getSpecs")
	public ResponseEntity<List<SpecializationDto>> showDoctorPage() {
		
		return ResponseEntity.ok(specService.getAllSpecIdAndName());
	}
//	
	@PostMapping("/save")
	public ResponseEntity<String> doctorSave(@RequestBody DoctorDto doctor) {
		
		Long id = doctorService.saveDoctor(doctor);
		
		String message = "Doctor '"+id+"' created";
		return ResponseEntity.ok(message);
	}
//	
	@GetMapping("/all")
	public ResponseEntity<List<DoctorDto>> showAllDoctor() {
		
		List<DoctorDto> doctors = doctorService.getAllDoctor();
		
		return ResponseEntity.ok(doctors);
	}
//	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<String> deleteDoctor(@PathVariable Long id) {
		
		doctorService.removeDoctor(id);
		String message = "Doctor '"+id+"' deleted";
		return ResponseEntity.ok(message);
		
	}
//	
	@GetMapping("/getDoctor/{id}")
	public ResponseEntity<DoctorDto> showDoctorEditPage(@PathVariable Long id) {
		
		DoctorDto doctor = doctorService.getOneDoctor(id);
		return ResponseEntity.ok(doctor);
	}
//	
	@PutMapping("/update")
	public ResponseEntity<String> doctorUpdate(@RequestBody DoctorDto doctor) {
		String msg = doctorService.updateDoctor(doctor);
		
		return ResponseEntity.ok(msg);
	}
//	
	

}
