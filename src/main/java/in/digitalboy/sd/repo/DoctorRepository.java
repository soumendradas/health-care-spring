package in.digitalboy.sd.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import in.digitalboy.sd.entity.Doctor;

public interface DoctorRepository extends JpaRepository<Doctor, Long> {

}
