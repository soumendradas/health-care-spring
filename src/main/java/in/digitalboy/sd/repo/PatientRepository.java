package in.digitalboy.sd.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import in.digitalboy.sd.entity.Patient;

public interface PatientRepository extends JpaRepository<Patient, Long> {

}
