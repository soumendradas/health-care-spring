package in.digitalboy.sd.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.digitalboy.sd.entity.Specialization;

public interface SpecializationRepository extends JpaRepository<Specialization, Integer> {

	@Query("SELECT specId, specName FROM Specialization")
	List<Object[]> getAllSpecIdAndName();
	
	@Query("SELECT COUNT(specCode) FROM Specialization WHERE specCode=:code")
	Long getSpecCodeCount(String code);
	
	@Query("SELECT COUNT(specCode) FROM Specialization WHERE specCode=:code AND specId!=:id")
	Long getSpecCodeCountForEdit(Integer id, String code);
	
	@Query("SELECT COUNT(specName) FROM Specialization WHERE specName=:name")
	Long getSpecNameCount(String name);
	
	@Query("SELECT COUNT(specName) FROM Specialization WHERE specName=:name AND specId!=:id")
	Long getSpecNameCountForEdit(Integer id, String name);
}
