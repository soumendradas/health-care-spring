package in.digitalboy.sd.config;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwagConfig {
	
	@Bean
	public Docket docs() {
		
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("in.digitalboy.sd"))
				.build()
				.apiInfo(getApi());
				
	}
	
	private ApiInfo getApi() {
		
		return new ApiInfo(
				"HealthCare REST API",
				"Documentation for HealthCare for 3rd party",
				"1.0",
				"Terms of Condition of the Healthcare",
				new Contact("Sd Soumendra","sdsoumendra.com", "soumendra.95@gmail.com" ),
				"MIT Licence",
				"https://opensource.org/licenses/MIT",
				new ArrayList<>()
				);
	}
	
	

}
