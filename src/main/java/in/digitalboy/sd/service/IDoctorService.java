package in.digitalboy.sd.service;

import java.util.List;

import in.digitalboy.sd.dto.DoctorDto;

public interface IDoctorService {

	Long saveDoctor(DoctorDto doctor);

	List<DoctorDto> getAllDoctor();

	void removeDoctor(Long id);

	DoctorDto getOneDoctor(Long id);

	String updateDoctor(DoctorDto doctor);

}
