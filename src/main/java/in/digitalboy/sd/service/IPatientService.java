package in.digitalboy.sd.service;

import java.util.List;

import in.digitalboy.sd.dto.PatientDto;

public interface IPatientService {

	Long savePatient(PatientDto patient);

	List<PatientDto> getAllPatient();

	void removePatient(Long id);

	PatientDto getOnePatient(Long id);

	String updatePatient(PatientDto patient);

}
