package in.digitalboy.sd.service;

import java.util.List;

import in.digitalboy.sd.dto.SpecializationDto;

public interface ISpecializationService {

	Integer saveSpec(SpecializationDto specialization);

	List<SpecializationDto> getAllSpec();

	void removeSpec(Integer id);

	SpecializationDto getOneSpec(Integer id);

	String updateSpec(SpecializationDto spec);
	List<SpecializationDto> getAllSpecIdAndName();
	
	boolean countSpecCode(Integer id, String code);
	
	boolean countSpecName(Integer id, String name);

}
