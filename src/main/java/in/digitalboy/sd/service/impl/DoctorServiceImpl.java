package in.digitalboy.sd.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.digitalboy.sd.dto.DoctorDto;
import in.digitalboy.sd.dto.SpecializationDto;
import in.digitalboy.sd.entity.Doctor;
import in.digitalboy.sd.exception.DoctorNotFoundExeception;
import in.digitalboy.sd.repo.DoctorRepository;
import in.digitalboy.sd.service.IDoctorService;
import in.digitalboy.sd.util.ModelUtil;

@Service
public class DoctorServiceImpl implements IDoctorService{
	
	@Autowired
	private DoctorRepository repo;
	
	private DoctorDto convertModelToDto(Doctor doctor) {
		
		return new DoctorDto(doctor);
	}
	
	
	
	@Override
	public Long saveDoctor(DoctorDto dto) {
		Doctor doctor = ModelUtil.converDoctorDtoToModel(dto);
		doctor = repo.save(doctor);
		return doctor.getDocId();
	}
	
	
	@Override
	public void removeDoctor(Long id) {
		if(repo.existsById(id)) {
			repo.deleteById(id);
		}
	}
	
	
	@Override
	public String updateDoctor(DoctorDto dto) {
		Doctor doctor = ModelUtil.converDoctorDtoToModel(dto);
		
		if(repo.existsById(doctor.getDocId())) {
			repo.save(doctor);
			return "Doctor '"+doctor.getDocId()+"' is updated";
		}
		
		return "ID is invalid";
		
		
	}


	@Override
	public List<DoctorDto> getAllDoctor() {
		List<Doctor> docs = repo.findAll();
		
		List<DoctorDto> dtos = new ArrayList<DoctorDto>();
		
		for(Doctor doc: docs) {
			dtos.add(convertModelToDto(doc));
		}
		
		return dtos;
	}


	@Override
	public DoctorDto getOneDoctor(Long id) {
		Doctor doc = repo.findById(id).orElseThrow(
				()-> new DoctorNotFoundExeception("ID is invalid"));
		return convertModelToDto(doc);
	}
}
