package in.digitalboy.sd.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.digitalboy.sd.dto.PatientDto;
import in.digitalboy.sd.entity.Patient;
import in.digitalboy.sd.exception.PatientNotFoundException;
import in.digitalboy.sd.repo.PatientRepository;
import in.digitalboy.sd.service.IPatientService;
import in.digitalboy.sd.util.ModelUtil;

@Service
public class PatientServiceImpl implements IPatientService {
	
	@Autowired
	private PatientRepository repo;
	
	@Override
	public Long savePatient(PatientDto patient) {
		
		
		return repo.save(ModelUtil.convertPatientDtoToModel(patient)).getId();
	}
	
	@Override
	public List<PatientDto> getAllPatient() {
		List<Patient> pats = repo.findAll();
		List<PatientDto> dtos = new ArrayList<>();
		
		for(Patient p : pats) {
			dtos.add(ModelUtil.convertPatientModelToDto(p));
		}
		
		return dtos;
	}

	@Override
	public void removePatient(Long id) {
		if(repo.existsById(id)) {
			repo.deleteById(id);
		}
	}

	@Override
	public PatientDto getOnePatient(Long id) {
		
		Patient p = repo.findById(id).orElseThrow(
				()-> new PatientNotFoundException("ID is invalid"));
		
		return ModelUtil.convertPatientModelToDto(p);
	}

	@Override
	public String updatePatient(PatientDto patient) {
		
		if(repo.existsById(patient.getPatId())) {
			repo.save(ModelUtil.convertPatientDtoToModel(patient));
			return "Patient '"+patient.getPatId()+"' is updated";
		}
		
		return "Id is invalid";
	}
	
	

}
