package in.digitalboy.sd.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.digitalboy.sd.dto.SpecializationDto;
import in.digitalboy.sd.entity.Specialization;
import in.digitalboy.sd.repo.SpecializationRepository;
import in.digitalboy.sd.service.ISpecializationService;
import in.digitalboy.sd.util.ModelUtil;

@Service
@Transactional
public class SpecializationServiceImpl implements ISpecializationService{
	
	@Autowired
	private SpecializationRepository repo;
	
	private SpecializationDto convertModelToDto(Specialization spec) {
		return new SpecializationDto(spec);
	}

	@Override
	public Integer saveSpec(SpecializationDto specialization) {
		// TODO Auto-generated method stub
		Specialization spec = ModelUtil.convertSpecializationDtoToModel(specialization);
		spec = repo.save(spec);
		return spec.getSpecId();
	}
	
	@Override
	public List<SpecializationDto> getAllSpec() {
		
		List<Specialization> specs = repo.findAll();
		List<SpecializationDto> dtos = new ArrayList<>();
		
		for(Specialization spec: specs) {
			dtos.add(convertModelToDto(spec));
		}
		
		return dtos;
	}
	
	@Override
	public void removeSpec(Integer id) {
		
		if(repo.existsById(id)) {
			repo.deleteById(id);
		}
		
	}
	
	@Override
	public SpecializationDto getOneSpec(Integer id) {
		// TODO Auto-generated method stub
		Specialization spec = repo.findById(id)
				.orElseThrow(()->new NoSuchElementException("Id is invalid"));
		
		return convertModelToDto(spec);
	}
	
	@Override
	public String updateSpec(SpecializationDto spec) {
		Specialization specialization = ModelUtil.convertSpecializationDtoToModel(spec);
		
		if(repo.existsById(specialization.getSpecId())) {
			repo.save(specialization);
			return "Specialization Id :"+ specialization.getSpecId()+" is updated";
		}
		
		return "Something wrong";
	}
	
	@Override
	public List<SpecializationDto> getAllSpecIdAndName() {
		List<Object[]> specs = repo.getAllSpecIdAndName();
		List<SpecializationDto> dtos = new ArrayList<>();
		for(Object[] ob:specs) {
			SpecializationDto dto = new SpecializationDto();
			dto.setSpecId(Integer.valueOf(ob[0].toString()));
			dto.setSpecName(ob[1].toString());
			dtos.add(dto);
		}
		
		return dtos;
	}
	
	@Override
	public boolean countSpecCode(Integer id, String code) {
		
		if(id != 0) {
			return repo.getSpecCodeCountForEdit(id, code)>0;
		}
		
		return repo.getSpecCodeCount(code)>0;
	}
	
	@Override
	public boolean countSpecName(Integer id, String name) {
		
		if(id!=0) {
			return repo.getSpecNameCountForEdit(id, name)>0;
		}
		
		return repo.getSpecNameCount(name)>0;
	}

}
