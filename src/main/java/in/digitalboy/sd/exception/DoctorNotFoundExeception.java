package in.digitalboy.sd.exception;

public class DoctorNotFoundExeception extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DoctorNotFoundExeception() {
		super();
	}

	public DoctorNotFoundExeception(String message) {
		super(message);
	}
	
}
