package in.digitalboy.sd.dto;

import in.digitalboy.sd.entity.Doctor;
import in.digitalboy.sd.entity.Specialization;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DoctorDto {
	
	private Long id;
	private String name, gender, email, mobile, address, note;
	
	private SpecializationDto specialization;
	public DoctorDto(Doctor doc) {
		super();
		this.id = doc.getDocId();
		this.name = doc.getDocName();
		this.specialization = new SpecializationDto(doc.getDocSpec());
		this.gender = doc.getDocGender();
		this.email = doc.getDocEmail();
		this.mobile = doc.getDocMobile();
		this.address = doc.getDocAddress();
		this.note = doc.getDocAddress();
	}
	
	
	

}
