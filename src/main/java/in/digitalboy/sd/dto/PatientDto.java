package in.digitalboy.sd.dto;

import java.time.LocalDate;
import java.util.Set;

import in.digitalboy.sd.entity.Patient;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PatientDto {
	
	private Long patId;
	private String firstName, lastName, gender, mobile,email, martialStatus, presAddress,
				commAddress, ifOther, note;
	
	private Set<String> medicalHist;
	
	private LocalDate dob;

	public PatientDto(Patient p) {
		super();
		this.patId = p.getId();
		this.firstName = p.getFirstName();
		this.lastName = p.getLastName();
		this.gender = p.getGender();
		this.mobile = p.getMobile();
		this.email = p.getEmail();
		this.martialStatus = p.getMartialStatus();
		this.presAddress = p.getPresentAddress();
		this.commAddress = p.getCommunicationAddress();
		this.ifOther = p.getIfOther();
		this.note = p.getNote();
		this.medicalHist = p.getMediHistory();
		this.dob = p.getDateOfBirth();
	}
	
	
}
