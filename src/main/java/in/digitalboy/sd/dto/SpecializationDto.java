package in.digitalboy.sd.dto;

import in.digitalboy.sd.entity.Specialization;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SpecializationDto {
	
	private Integer specId;
	private String specCode;
	private String specName;
	private String specNote;
	
	public SpecializationDto(Specialization spec) {
		super();
		this.specId = spec.getSpecId();
		this.specCode = spec.getSpecCode();
		this.specName = spec.getSpecName();
		this.specNote = spec.getSpecNote();
	}
	
	

}
