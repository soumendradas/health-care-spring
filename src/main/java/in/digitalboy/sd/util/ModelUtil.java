package in.digitalboy.sd.util;

import in.digitalboy.sd.dto.DoctorDto;
import in.digitalboy.sd.dto.PatientDto;
import in.digitalboy.sd.dto.SpecializationDto;
import in.digitalboy.sd.entity.Doctor;
import in.digitalboy.sd.entity.Patient;
import in.digitalboy.sd.entity.Specialization;

public class ModelUtil {
	
	public static Specialization convertSpecializationDtoToModel(SpecializationDto spec) {
		Specialization s = new Specialization();
		s.setSpecId(spec.getSpecId());
		s.setSpecCode(spec.getSpecCode());
		s.setSpecName(spec.getSpecName());
		s.setSpecNote(spec.getSpecNote());
		
		return s;
	}
	
	public static Doctor converDoctorDtoToModel(DoctorDto dto) {
		Doctor doc = new Doctor();
		doc.setDocId(dto.getId());
		doc.setDocName(dto.getName());
		doc.setDocEmail(dto.getEmail());
		doc.setDocGender(dto.getGender());
		doc.setDocMobile(dto.getMobile());
		doc.setDocNote(dto.getNote());
		doc.setDocAddress(dto.getAddress());
		doc.setDocSpec(convertSpecializationDtoToModel(dto.getSpecialization()));
		
		return doc;
	}
	
	public static Patient convertPatientDtoToModel(PatientDto dto) {
		Patient p = new Patient(dto);
		
		return p;
	}
	
	public static PatientDto convertPatientModelToDto(Patient p) {
		
		PatientDto dto = new PatientDto(p);
		return dto;
	}

}
