package in.digitalboy.sd.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import lombok.Data;

@Entity
@Data
@Table(name = "doc_tab")
public class Doctor {
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "key_gen")
	@TableGenerator(name = "key_gen",
					pkColumnName = "key_name",
					pkColumnValue = "doc_id",
					valueColumnName = "key_value")
	@Column(name = "doc_id")
	private Long docId;
	
	@Column(name = "doc_name")
	private String docName;
	
	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinColumn(name = "spec_id")
	private Specialization docSpec;
	
	@Column(name = "doc_gender")
	private String docGender;

	
	@Column(name = "doc_email")
	private String docEmail;
	
	@Column(name = "doc_mob")
	private String docMobile;
	
	@Column(name = "doc_addr")
	private String docAddress;
	
	
	@Column(name = "doc_note", length = 500)
	private String docNote;
	
}
