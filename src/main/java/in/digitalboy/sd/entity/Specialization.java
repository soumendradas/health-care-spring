package in.digitalboy.sd.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "spec_tab")
public class Specialization {
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "key_gen")
	@TableGenerator(name = "key_gen",
					pkColumnName = "key_name",
					pkColumnValue = "spec_id",
					valueColumnName = "key_value",
					allocationSize = 1)
	@Column(name="sp_id")
	private Integer specId;
	
	@Column(name = "sp_code", length = 10)
	private String specCode;
	
	@Column(name = "sp_name", length = 50)
	private String specName;
	
	@Column(name = "sp_note", length = 500)
	private String specNote;
	

}
